rgarrigue.python
================

Install Python 2 and/or 3 plus pip, and pip packages.

Requirements
------------

This role is meant to work and tested for Ubuntu, Debian, RedHat & co including Fedora. RedHat, CentOS, Oracle Linux and Scientific Linux requires EPEL repository, hence `rgarrigue.yum-repositories` is in the role's dependencies, but that doesn't affect Debian or Ubuntu OSes.

Role Variables
--------------

By default Python 3 and pip3 are installed. This can be changed with `python2_enabled: true` and `python3_enabled: false`.

Additionnal pip packages to be installed can be specified like this:

```yaml
pip_packages:
  - name: testinfra
    version: 1.7.0
  - name: tox
  - name: Sphinx
    version: latest
```

Dependencies
------------

* rgarrigue.yum-repositories

Example Playbook
----------------

```yaml
- hosts: all
  vars:
    python2_enabled: true
    python3_enabled: false
    pip_packages:
      - name: flask
  roles:
     - rgarrigue.python
```

Tests
-----

This role is tested using [Molecule](https://molecule.readthedocs.io/en/latest/) & [Travis CI](http://travis-ci.org/), installing both python 2 and 3 plus so;e pip packages [![Build Status](https://travis-ci.org/rgarrigue/ansible-role-python.svg?branch=master)](https://travis-ci.org/rgarrigue/ansible-role-python)

For manual test, [install molecule deps](https://github.com/rgarrigue/ansible-role-python/blob/master/molecule/default/INSTALL.rst) then run `molecule test`

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue
