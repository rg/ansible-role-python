import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_pip_is_installed(host):
    if host.system_info.distribution == 'centos' \
            or host.system_info.distribution == 'ol':
        pip_package_name = 'python34-pip'
    else:
        pip_package_name = 'python3-pip'

    pip = host.package(pip_package_name)
    assert pip.is_installed


def test_pip_packages(host):

    packages = host.pip_package.get_packages(pip_path='/usr/bin/pip3')

    assert 'testinfra' in packages
    assert packages['testinfra']['version'] == '1.7.0'
    assert 'tox' in packages
    assert 'Sphinx' in packages
